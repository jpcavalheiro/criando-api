const express = require('express');
const cors = require('cors')
const server = express();
server.use(express.json());
server.use(cors())

const users = [ 
    {
    nome: "Felipe"
    },
    {
    nome: "Lucas"
    },
    {
    nome: "Gustavo"
    }
];

server.get("/users", (req, res)=>{
    res.send(users)
});

server.get("/users/:index", (req, res) =>{
    console.log(req.params);
    const { index } = req.params;
    return res.send({user: users[index]})
});

server.post("/users", (req, res) =>{
    const { name } = req.body;
    console.log(name);
    users.push({nome: name});
});

server.put("/users", (req, res) =>{
    const { id } = req.query;
    const { name } = req.body;
    users[id] = {nome: name};
    return res.send( users );
});

server.delete("/users", (req, res) => {
    const { id } = req.body;
    console.log(id)
    users.splice(id, 1);
    res.send(users);
  });

server.listen(3000);